INSERT INTO public.user_details (id, password, username) VALUES (1, '$2a$10$fDkYPmOvngJ/slSv3zBBX.pvlvTqcE8kuEpr23DmBZtqDAlvrI./a', 'fulano'); -- senha: secret
INSERT INTO public.user_details (id, password, username) VALUES (2, '$2a$10$fDkYPmOvngJ/slSv3zBBX.pvlvTqcE8kuEpr23DmBZtqDAlvrI./a', 'beltrano'); -- senha: secret
INSERT INTO public.profile (id, birthday, email, full_name, phone, user_id) VALUES (1, '1994-10-11', 'fulano@teste.com', 'Fulano de tal', '99999999999', 1);
INSERT INTO public.profile (id, birthday, email, full_name, phone, user_id) VALUES (2, '1994-10-11', 'beltrano@teste.com', 'Beltrano de tal', '99999999991', 2);

INSERT INTO public.company (id, name, description, token) VALUES (1, 'Google', 'O grande buscador', '$2a$10$fDkYPmO');
INSERT INTO public.company (id, name, description, token) VALUES (2, 'Microsoft', 'Nem dou mais tanto tela azul', 'fDkYPmOvngJ/s');
INSERT INTO public.company (id, name, description, token) VALUES (3, 'Nvidea', 'Bora joga?', 'BBX.pvlvTqcE8kuE');