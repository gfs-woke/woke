package com.woke.service;

import com.woke.model.Company;
import com.woke.model.Profile;
import com.woke.model.UserDetailsImpl;
import com.woke.repository.CompanyRepository;
import com.woke.repository.ProfileRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class CompanyServiceImpl implements CompanyService {

    private final CompanyRepository repository;
    private final ProfileRepository pRepo;

    public List<Company> findAll(){
        return repository.findAll();
    }

    public List<Company> findAvailableCompanies(){
        UserDetailsImpl principal = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Profile profile = pRepo.findByUser(principal);
        return repository.findAllByProfilesNotContains(profile);
    }

    @Override
    public Company save(Company company) {
        return repository.save(company);
    }

    public  Optional<List<Company>> findAllByProfilesContains(Profile profile){
        return repository.findAllByProfilesContains(profile);
    }

    public Optional<Company> findByToken(String token) {
        return repository.findByToken(token);
    }

}
