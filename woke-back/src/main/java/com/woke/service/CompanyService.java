package com.woke.service;

import com.woke.model.Company;
import com.woke.model.Profile;

import java.util.List;
import java.util.Optional;

public interface CompanyService {
    List<Company> findAll();
    List<Company> findAvailableCompanies();
    Company save(Company company);
    Optional<List<Company>> findAllByProfilesContains(Profile profile);
    Optional<Company> findByToken(String token);
}
