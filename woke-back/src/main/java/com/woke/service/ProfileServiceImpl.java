package com.woke.service;

import com.woke.model.Profile;
import com.woke.model.UserDetailsImpl;
import com.woke.repository.ProfileRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ProfileServiceImpl implements ProfileService{

    private final ProfileRepository repository;

    @Override
    public Optional<Profile> findByUsername(String username) {
        return repository.findByUserUsername(username);
    }

    @Override
    public Profile updateProfile(Profile profile) {
        UserDetailsImpl principal = getPrincipal();
        Profile userProfile = findByUsername(principal.getUsername()).orElseThrow(() -> {
            throw new RuntimeException("Usuario logado sem perfil cadastrado.");
        });

        if (!profile.getId().equals(userProfile.getId())) {
            throw new RuntimeException("Perfil não pertence ao usuario logado");
        }

        profile.setUser(principal);

        return repository.save(profile);
    }

    public Profile findLoggedProfile() {
        return repository.findByUser(getPrincipal());
    }

    private UserDetailsImpl getPrincipal() {
        return (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }
}
