package com.woke.service;

import com.woke.model.Profile;

import java.util.Optional;

public interface ProfileService {

    Optional<Profile> findByUsername(String username);

    Profile updateProfile(Profile profile);

    Profile findLoggedProfile();

}
