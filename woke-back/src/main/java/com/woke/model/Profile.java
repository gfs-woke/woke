package com.woke.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import java.time.LocalDate;

@Data
@Entity
@NoArgsConstructor
public class Profile {
    @Id
    @GeneratedValue
    private Long id;

    private String fullName;
    private String phone;
    private String email;

    @JsonFormat(pattern = "dd/MM/yyyy")
    private LocalDate birthday;

    @OneToOne
    @JsonIgnore
    private UserDetailsImpl user;

    public Profile(Long id) {
        this.id = id;
    }
}
