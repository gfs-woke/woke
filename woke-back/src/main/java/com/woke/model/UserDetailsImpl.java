package com.woke.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Table(name = "user_details")
public class UserDetailsImpl implements UserDetails {

    @Id
    @SequenceGenerator(name = "user_seq", sequenceName = "user_seq", allocationSize = 1)
    @GeneratedValue(generator = "user_seq", strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(unique = true)
    private String username;

    @JsonIgnore
    private String password;

    @Transient
    private Set<Role> authorities = new HashSet<>();

    @Override
    public boolean isAccountNonExpired() {
        //Method not implemented yet
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        //Method not implemented yet
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        //Method not implemented yet
        return true;
    }

    @Override
    public boolean isEnabled() {
        //Method not implemented yet
        return true;
    }
}
