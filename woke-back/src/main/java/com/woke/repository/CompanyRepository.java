package com.woke.repository;

import com.woke.model.Company;
import com.woke.model.Profile;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CompanyRepository extends JpaRepository<Company, Long> {
    List<Company> findAllByProfilesNotContains(Profile profile);
    Optional<List<Company>> findAllByProfilesContains(Profile profile);
    Optional<Company> findByToken(String token);
}
