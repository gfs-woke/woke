package com.woke.repository;

import com.woke.model.Profile;
import com.woke.model.UserDetailsImpl;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ProfileRepository extends JpaRepository<Profile, Long> {

    Optional<Profile> findByUserUsername(String username);
    Profile findByUser(UserDetailsImpl user);

}
