package com.woke;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.CrossOrigin;

//TODO: setup cross origem
@SpringBootApplication
@CrossOrigin(origins = "*")
public class WokeBackApplication {

    public static void main(String[] args) {
        SpringApplication.run(WokeBackApplication.class, args);
    }

}
