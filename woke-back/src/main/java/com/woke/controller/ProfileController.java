package com.woke.controller;

import com.woke.model.Profile;
import com.woke.service.ProfileService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/profile")
public class ProfileController {

    private final ProfileService service;

    @GetMapping("/{username}")
    public ResponseEntity<Profile> getByUser(@PathVariable String username) {
        return ResponseEntity.of(service.findByUsername(username));
    }

    @PostMapping
    public ResponseEntity<Profile> updateProfile(@RequestBody Profile profile) {
        return ResponseEntity.ok(service.updateProfile(profile));
    }

}
