package com.woke.controller;

import com.woke.model.Company;
import com.woke.model.Profile;
import com.woke.service.CompanyService;
import com.woke.service.ProfileService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
@RequestMapping("/company")
public class CompanyController {

    private final CompanyService service;
    private final ProfileService profileService;

    @GetMapping
    public ResponseEntity<List<Company>> findAll() {
        return ResponseEntity.ok(service.findAvailableCompanies());
    }

    @PostMapping("/profile")
    public ResponseEntity<Company> addProfile(@RequestBody Company company) {
        //TODO: verifica a company
        company.getProfiles().add(profileService.findLoggedProfile());
        return ResponseEntity.ok(service.save(company));
    }

    @GetMapping("/{profileId}")
    public ResponseEntity<List<Company>> getMyCompanies(@PathVariable Long profileId) {
        return ResponseEntity.of(service.findAllByProfilesContains(new Profile(profileId)));
    }

    @GetMapping("/candidates/profile/{profileId}")
    public ResponseEntity<Profile> getProfileByCompany(@RequestParam String tk, @PathVariable Long profileId) {

        Optional<Company> opCompany = service.findByToken(tk);
        if (opCompany.isEmpty()){
            return ResponseEntity.notFound().build();
        }

        Optional<Profile> first = opCompany.get().getProfiles().stream()
                .filter(profile -> profile.getId().equals(profileId))
                .findFirst();

        return ResponseEntity.of(first);
    }

}
