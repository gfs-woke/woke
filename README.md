# Woke teste
O projeto foi construido com um front-end escrito em TypeScript utilizando o framework Angular, 
e um back-end escrito em java utilizando o framework springboot.

Para o banco de dados foi utilizado o Postgres

Também está preparado para rodar em containers.

### Back-end
No back-end foi utilizado: 
 - spring web para gerenciamento dos endpoints;
 - spring-security para segurança da aplicação e validação do token jwt;
 - hibernate e spring jpa para acesso ao banco de dados;
 - Swagger para documentação da api: http://localhost:8080/api/swagger-ui/
 - maven para gerenciamento de dependência e geração de artefatos

O back-end tem como dependência um banco de dados Postgres com uma base woke criada. 
O sistema cria as tabelas e as popula com os dados necessários no start da aplicação. 
Caso necessário a um dump da base em docs/dump.sql. 

O back-end conta com as seguintes variáveis disponíveis:
 - JWT_SECRET: utilizado para gerar o jwt
 - JWT_EXP_SEC: tempo de expiração do jwt em segundos
 - JDBC_URL: url do banco de dados
 - JDBC_USER: usuário do banco
 - JDBC_PASSWORD: password do banco
 - DLL_AUTO: cria a base no start, valores disponíveis: create-drop, create, none, update e validate
 - SHOW_SQL: mostra as consultas no log da aplicação

### Front-end
Para a construção do front foi utilizado: 
 - Angular 12.0.5
 - Materialize-css 
 - jquery 3.6.0
 - google-fonts
 - ngx-mask

Foi executado utilizando node v14.17.1 e npm 6.14.13.

Para as chamadas ao back-end em modo dev foi utilizado um proxy conforme config contida no arquivo woke-front/src/proxy.conf.json. 

Para a execução em "prod" foi utilizado o nginx para servir a aplicação com a config contida em woke-front/default.conf.template. 
O host e port do back-end são configurados através das variáveis HOST e PORT.

Foram cadastrados dois usuários: fulano e beltrano, ambos com o password secret.

## Para executar
Para rodar o projeto pode ser utilizado o docker-compose.yaml disponível na raiz do projeto. 
```bash
git clone https://gitlab.com/gfs-woke/woke.git
docker-compose up -d
```

Caso necessário os projetos podem ser executados separadamente:
```bash
#Back
cd woke-back
./mvnw clean package -DskipTests
cd target
java -jar woke.jar

# Front
cd woke-front
npm install
npm run start
```

Após o start das aplicações o front-end estará disponível através de localhost:4200 e o back em localhost:8080  