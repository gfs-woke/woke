import {Profile} from "./profile";

export class Company {
  id: number;
  name: string;
  description: string;
  profiles: Profile[];

  constructor(id: number, name: string, description: string, profiles: Profile[]) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.profiles = profiles;
  }
}
