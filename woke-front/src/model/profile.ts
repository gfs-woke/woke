import {Company} from "./company";

export class Profile {
  id?: number
  fullName?: string;
  phone?: string;
  email?: string;
  birthday?: string;
  companies?: Company[]

  constructor(id?: number, fullName?: string, phone?: string, email?: string, birthday?: string, companies?: Company[]) {
    this.id = id;
    this.fullName = fullName;
    this.phone = phone;
    this.email = email;
    this.birthday = birthday;
    this.companies = companies;
  }
}
