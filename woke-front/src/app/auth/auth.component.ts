import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {AuthService} from "./auth.service";
import {Router} from "@angular/router";

declare const M: any;

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {

  loginForm = this.fb.group({
    username: ['', Validators.required],
    password: ['', Validators.required]
  })

  constructor(private fb: FormBuilder, private service: AuthService, private router: Router) {
  }

  ngOnInit(): void {
  }

  doLogin() {
    const val = this.loginForm.value;
    if (val.username && val.password) {
      this.service.login(val.username, val.password).subscribe(
        (user) => {
          M.toast({html: 'Bem vindo de volta: ' + user?.username})
          this.router.navigateByUrl('/home');
        },
        error => {
          if (error.status === 401) {
            M.toast({html: 'Login ou senha incorretos!'});
          } else {
            M.toast({html: 'Algo deu errado, tente novamente dentro de alguns instantes.'})
          }
        }
      );

    }
  }

}
