import {Injectable} from '@angular/core';
import {User} from "../../model/user";
import {map} from "rxjs/operators";
import {ApiService} from "../shared/services/api.service";
import * as moment from 'moment';
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private api: ApiService, private route: Router) {
  }

  login(username: string, password: string) {
    return this.api.post('/api/auth', {username, password}, {observe: 'response'}).pipe(map(res => {
      this.setSession(res);
      let user = new User(res.body.username, res.body.password);
      sessionStorage.setItem('currentUser', JSON.stringify(user));
      return user;
    }))
  }

  public getCurrentUser(): User {
    return JSON.parse(<string>sessionStorage.getItem('currentUser'));
  }

  private setSession(authResult: any) {
    const headers = authResult.headers;
    const expiresAt = moment().add(headers.get('Access-Control-Max-Age'), 'second');

    sessionStorage.setItem('token', headers.get('Authorization'));
    sessionStorage.setItem('expires_at', JSON.stringify(expiresAt.valueOf()));

  }

  logout() {
    sessionStorage.removeItem('token');
    sessionStorage.removeItem('expires_at');
    sessionStorage.removeItem('currentUser');
    this.route.navigate(["/login"]);
  }

  public isLoggedIn() {
    const token = localStorage.getItem('token') || sessionStorage.getItem('token');
    return token != null && moment().isBefore(this.getExpiration());
  }

  isLoggedOut() {
    return !this.isLoggedIn();
  }

  public getToken(): string {
    return <string>sessionStorage.getItem('token');
  }

  getExpiration() {
    const expiration = localStorage.getItem('expires_at') || sessionStorage.getItem('expires_at');
    if (expiration != null) {
      const expiresAt = JSON.parse(expiration);
      return moment(expiresAt);
    }
    return null;
  }


}
