import {Component, OnInit} from '@angular/core';
import {User} from "../../model/user";
import {AuthService} from "../auth/auth.service";
import {Profile} from "../../model/profile";
import {FormBuilder, Validators} from "@angular/forms";
import {ProfileService} from "../shared/services/profile.service";
import * as moment from "moment";
import {Company} from "../../model/company";
import {CompanyService} from "../shared/services/company.service";

declare const $: any;
declare const M: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  //TODO: Separar cada card em um componente
  user: User = new User();
  profile: Profile = new Profile();
  companies: Company[] = [];

  pForm = this.fb.group({
    id: [''],
    fullName: ['', [Validators.required, Validators.minLength(3)]],
    email: ['', [Validators.required, Validators.email]],
    phone: ['', Validators.required],
    birthday: ['', [Validators.required]]
  })

  compForm = this.fb.group({
    company: ['', Validators.required]
  })

  constructor(
    private auth: AuthService,
    private service: ProfileService,
    private fb: FormBuilder,
    private cpService: CompanyService) {
  }

  //TODO: Create delete to send profile list
  sendProfile() {
    let company = this.compForm.value['company'];
    this.cpService.sendProfile(company).subscribe(value => {
      this.companies = this.companies.filter(value => value !== company);
      this.profile.companies?.push(company);
      this.compForm.reset();
      this.createSelect();
      M.toast({html: 'Perfil enviado com sucesso!'})
    }, error => {
      console.error(error);
      M.toast({html: 'Falha ao enviar perfil, tente novamente em instantes.'})
    })
  }

  updateProfile() {
    this.profile = this.pForm.value;
    const birthday = moment($('.datepicker').val(), "DD MMMM, YYYY", 'pt-br')
    this.profile.birthday = birthday.format("DD/MM/YYYY");
    this.service.updateProfile(this.profile).subscribe(value => {
        M.toast({html: 'Perfil atualizado com sucesso!'})
      },
      error => {
        console.error(error);
        M.toast({html: 'Falha ao atualizar o perfil, tente novamente em instantes.'})
      })
  }

  ngOnInit(): void {
    this.user = this.auth.getCurrentUser();
    this.service.getProfile().subscribe(value => {
      this.profile = value;
      this.pForm.patchValue(value);
      this.initMaComp();
      this.createSelect();
      this.cpService.getMyCompanies(this.profile.id || 0).subscribe(comps => {
        this.profile.companies = comps;
      })
    });
    this.cpService.getAllCompanies().subscribe(value => {
      this.companies = value;
    });
  }

  private initMaComp() {
    const birthday = moment(this.profile.birthday?.toString(), "DD/MM/YYYY", 'pt-br');
    $(document).ready(function () {
      $('.datepicker').datepicker({
        format: 'dd mmmm, yyyy',
        yearRange: 50,
        i18n: {
          months: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
          monthsShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
          weekdays: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sabádo'],
          weekdaysShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
          weekdaysAbbrev: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
          today: 'Hoje',
          clear: 'Limpar',
          cancel: 'Sair',
          done: 'Confirmar',
          labelMonthNext: 'Próximo mês',
          labelMonthPrev: 'Mês anterior',
          labelMonthSelect: 'Selecione um mês',
          labelYearSelect: 'Selecione um ano',
          selectMonths: true,
          selectYears: 15,
        },
        maxDate: new Date(),
        defaultDate: birthday.toDate(),
        setDefaultDate: true
      });
      M.updateTextFields();
    });
  }

  private createSelect() {
    setTimeout(()=>{
      // $('select').formSelect('destroy');
      $('select').formSelect();
    }, 500);
  }

}
