import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthComponent} from "./auth/auth.component";
import {HomeComponent} from "./home/home.component";
import {AuthGuard} from "./guard/auth.guard";
import {LoginGuard} from "./guard/login.guard";

const routes: Routes = [
  {path: "login", component: AuthComponent, canActivate: [LoginGuard]},
  {path: "home", component: HomeComponent, canActivate: [AuthGuard]},
  {path: "", redirectTo: "login", pathMatch: "full"},
  //TODO: Criar error pages
  {path: "**", redirectTo: "home"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
