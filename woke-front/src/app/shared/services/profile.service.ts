import {Injectable} from '@angular/core';
import {ApiService} from "./api.service";
import {Observable} from "rxjs";
import {Profile} from "../../../model/profile";
import {AuthService} from "../../auth/auth.service";

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor(private api: ApiService, private auth: AuthService) { }

  public getProfile(): Observable<Profile> {
    return this.api.get("/api/profile/" + this.auth.getCurrentUser().username);
  }

  public updateProfile(profile: Profile) {
    return this.api.post("/api/profile", profile);
  }

}
