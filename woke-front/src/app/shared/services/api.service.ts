import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private options = {
    withCredentials: true,
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
    }),
  };

  constructor(private http: HttpClient) {}

  public post(url: string, body: any, options?: any): Observable<any> {
    return this.http.post(url, body, options ? options : this.options);
  }

  public put(url: string, body: any, options?: any): Observable<any> {
    return this.http.put(url, body, options ? options : this.options);
  }

  public get(url: string, options?: any): Observable<any> {
    return this.http.get(url, options ? options : this.options);
  }

}
