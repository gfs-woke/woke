import {Injectable} from '@angular/core';
import {ApiService} from "./api.service";
import {Observable} from "rxjs";
import {Company} from "../../../model/company";

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  constructor(private api: ApiService) { }

  public getAllCompanies(): Observable<Company[]> {
    return this.api.get("/api/company");
  }

  public sendProfile(company: Company) {
    return this.api.post("/api/company/profile", company);
  }

  public getMyCompanies(profileId: number): Observable<Company[]> {
    return this.api.get(`/api/company/${profileId}`);
  }

}
